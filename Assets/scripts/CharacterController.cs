﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {
    private float verSpeed = 1.0f;
    private float horSpeed = 1.0f;
    private float ver;
    private float hor;
    private float groundDistance = 0.3f;
    private float jumpForce = 500.0f;
    private float grabDistance = 1.2f;

    private Animator anim;
    private Rigidbody rb;

    public LayerMask isGround;
    // public Transform rightArm;

    public Transform lookObj;
    public Transform rightHandObj;
    public bool ikActive = false;
    private GameObject objectHolder;

	// Use this for initialization
	void Start () {
        this.anim = GetComponent<Animator>();
        this.rb = GetComponent<Rigidbody>();
        this.objectHolder = GameObject.Find("ObjHolder");
	}
	
	// Update is called once per frame
	void Update () {
        ver = Input.GetAxis("Vertical") * this.verSpeed;
        hor = Input.GetAxis("Horizontal") * this.horSpeed;
        this.anim.SetFloat("speed", ver);
        this.anim.SetFloat("turnSpeed", hor);

        this.rightHandObj.LookAt(this.rightHandObj.transform.position - this.transform.position);

        Vector3 dirFromObj = (this.rightHandObj.transform.position - this.transform.position).normalized;
        float lookingAtObj = Vector3.Dot(dirFromObj, this.transform.forward);

        // Jumping
        if(Input.GetButtonDown("Jump"))
        {
            //rb.AddForce(Vector3.up * jumpForce);
            rb.AddForce(0, jumpForce, jumpForce, ForceMode.Acceleration);
            this.anim.SetTrigger("jump");
        }

        // Checking if on ground
        if(Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, groundDistance, isGround))
        {
            this.anim.SetBool("grounded", true);
        } else
        {
            this.anim.SetBool("grounded", false);
        }

        // Brazilian Kick
        if (Input.GetKeyDown(KeyCode.F))
        {
            this.anim.SetTrigger("brazilian_kick");
        }

        // Front Kick
        if (Input.GetKeyDown(KeyCode.G))
        {
            this.anim.SetTrigger("front_kick");
        }

        // Axe Kick (turned into headbut using mask)
        if (Input.GetKeyDown(KeyCode.H))
        {
            this.anim.SetTrigger("axe_kick");
        }

        // Grap item
        if (lookingAtObj >= 0.5)
        {
            if (Input.GetKey(KeyCode.E))
            {
                this.ikActive = true;
                if (Vector3.Distance(this.transform.position, this.lookObj.position) <= this.grabDistance)
                {
                    this.lookObj.parent = this.objectHolder.transform;
                    this.lookObj.transform.localPosition = new Vector3(0.14f, 0.05f, -0.25f); ;
                    this.lookObj.transform.localEulerAngles = new Vector3(260, 15, -50);
                    this.anim.SetBool("hasWeapon", true);
                }
            }
            else
            {
                this.ikActive = false;
            }
        }
        else
        {
            this.ikActive = false;
        }

        // Drop item
        if (Input.GetKeyDown(KeyCode.Q))
        {
            this.lookObj.parent = null;
            this.anim.SetBool("hasWeapon", false);
        }

        // Sword Attack
        if (Input.GetMouseButton(0))
        {
            this.anim.SetTrigger("attack");
        }
    }

    private void OnAnimatorIK()
    {
        if(anim)
        {
            if(ikActive)
            {
                if (lookObj != null)
                {
                    this.anim.SetLookAtWeight(1);
                    this.anim.SetLookAtPosition(lookObj.position);
                }

                if(rightHandObj != null)
                {
                    this.anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                    this.anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                    this.anim.SetIKPosition(AvatarIKGoal.RightHand, rightHandObj.position);
                    this.anim.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation);
                }
            }
            else
            {
                this.anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                this.anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                this.anim.SetLookAtWeight(0);
            }
        }
    }
}
